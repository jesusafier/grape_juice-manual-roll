# grape_juice-manual-roll

Just a proof-of-concept for hooking into the roll system on pre 0.7.x

Enabling the module will cause any roll you attempt to make open up a prompt that requests the outcome of a die roll.

<h1>Install</h1>
https://gitlab.com/jesusafier/grape_juice-manual-roll/-/jobs/artifacts/master/raw/module.json?job=build-module

Join the discussions on our [Discord](https://discord.gg/467HAfZ)
and if you liked it, consider supporting (and getting EA) me on [patreon](https://www.patreon.com/foundry_grape_juice)